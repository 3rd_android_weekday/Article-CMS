package com.kshrd.articlecms;

import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.kshrd.articlecms.entity.ArticleResponse;
import com.kshrd.articlecms.entity.UpdateArticleResponse;
import com.kshrd.articlecms.event.ArticleLoadMoreEvent;
import com.kshrd.articlecms.event.ArticleUpdateEvent;
import com.kshrd.articlecms.form.UpdateArticleForm;
import com.kshrd.articlecms.webservice.ArticleService;
import com.kshrd.articlecms.webservice.ServiceGenerator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements TextWatcher, MyClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rvArticle)
    RecyclerView rvArticle;

    @BindView(R.id.etKeyword)
    EditText etKeyword;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    ArticleAdapter articleAdapter;
    ArticleService articleService;

    Handler handler;
    Runnable runnable;

    // Pagination
    int page = 1;

    private CompositeDisposable compositeDisposable;
    private int totalPage;
    private static final int ITEM_PER_PAGE = 15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        compositeDisposable = new CompositeDisposable();

        // Initialization
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                page = 1;
                loadArticleByPagination(page, etKeyword.getText().toString());
            }
        };

        setRecyclerView();

        articleService = ServiceGenerator.createService(ArticleService.class);
        loadArticleByPagination(page, etKeyword.getText().toString());
        etKeyword.addTextChangedListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);


        // Code..

    }

    private void loadArticles(String keyword) {
//        Call<ArticleResponse> call = articleService.findArticleByTitle(keyword);
//        call.enqueue(new Callback<ArticleResponse>() {
//            @Override
//            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
//
//                ArticleResponse articleResponse = response.body();
//                articleAdapter.clearList();
//                articleAdapter.addMoreItems(articleResponse.getArticlelist());
//
//            }
//
//            @Override
//            public void onFailure(Call<ArticleResponse> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });

        Observable<ArticleResponse> findArticle = articleService.findArticleByTitle(keyword);
        compositeDisposable.add(
                findArticle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArticleResponse>(){

                    @Override
                    public void onNext(ArticleResponse articleResponse) {
                        articleAdapter.clearList();
                        articleAdapter.addMoreItems(articleResponse.getArticlelist());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.e("ooooo", "Complete");
                    }
                })
        );
    }

    private void setRecyclerView() {
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        articleAdapter = new ArticleAdapter(rvArticle);
        rvArticle.setAdapter(articleAdapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence keyword, int i, int i1, int i2) {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onClicked(int position, View view) {
        final ArticleResponse.Article article = articleAdapter.getArticle(position);
        PopupMenu popupMenu = new PopupMenu(MainActivity.this, view);
        popupMenu.inflate(R.menu.my_menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        Toast.makeText(MainActivity.this, String.valueOf(article.getId()), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.update:
                        DialogFragment fragment = MyDialogFragment.newInstance(article);
                        fragment.show(getSupportFragmentManager(), "MyDialogFragment");


                        break;
                }
                return true;
            }

        });
        popupMenu.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onArticleUpdateEvent(ArticleUpdateEvent event){
        final ArticleResponse.Article article = event.getArticle();
        UpdateArticleForm form = new UpdateArticleForm(
            article.getTitle(),
                article.getDescription(),
                article.getAuthor().getId(),
                article.getCategory().getId(),
                article.getStatus(),
                article.getImage()
        );

        Call<UpdateArticleResponse> updateArticle = articleService.updateArticle(article.getId(), form);
        updateArticle.enqueue(new Callback<UpdateArticleResponse>() {
            @Override
            public void onResponse(Call<UpdateArticleResponse> call, Response<UpdateArticleResponse> response) {
                articleAdapter.updateItemOf(response.body().getArticle());
            }

            @Override
            public void onFailure(Call<UpdateArticleResponse> call, Throwable t) {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecyclerArticleLoadMore(ArticleLoadMoreEvent event){

        if (page == totalPage){
            return;
        }

        articleAdapter.addProgressBar();
        loadArticleByPagination(++page, etKeyword.getText().toString());
    }

    void loadArticleByPagination(final int page, String title){

        compositeDisposable.add(
                articleService.findArticleByPagination(page, title)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArticleResponse>() {
                    @Override
                    public void onSuccess(final ArticleResponse articleResponse) {

                        if (swipeRefreshLayout.isRefreshing()){
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        if (page == 1) {
                            articleAdapter.clearList();
                        }

                        totalPage = articleResponse.getPagination().getTotalPages();

                        if (articleAdapter.isLoading() && articleAdapter.size() > ITEM_PER_PAGE){
                            articleAdapter.removeProgressBar();
                        }

                        articleAdapter.addMoreItems(articleResponse.getArticlelist());
                        articleAdapter.onLoaded();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                })
        );

    }

    @Override
    public void onRefresh() {
        page = 1;
        etKeyword.setText("");
        loadArticleByPagination(page, etKeyword.getText().toString());
    }
}
