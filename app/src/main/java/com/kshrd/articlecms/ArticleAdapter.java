package com.kshrd.articlecms;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kshrd.articlecms.entity.ArticleResponse;
import com.kshrd.articlecms.event.ArticleLoadMoreEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pirang on 6/14/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ArticleResponse.Article> articleList;
    MyClickListener clickListener;
    private int visibleThreshold = 5;
    private boolean loading = false;

    private static final int VIEW_ITEM = 1;
    private static final int VIEW_PROG = 0;

    public ArticleAdapter(RecyclerView recyclerView) {
        articleList = new ArrayList<>();

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do Something
                        loading = true;
                        EventBus.getDefault().post(new ArticleLoadMoreEvent());
                    }
                }
            });
        }
    }

    public void onLoaded() {
        loading = false;
    }

    public boolean isLoading(){
        return loading;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        clickListener = (MyClickListener) recyclerView.getContext();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh = null;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom, parent, false);
            vh = new ArticleViewHolder(view);
        } else if (viewType == VIEW_PROG) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progressbar, parent, false);
            vh = new ProgressBarViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder articleViewHolder = (ArticleViewHolder) holder;
            ArticleResponse.Article article = articleList.get(position);
            articleViewHolder.tvTitle.setText(article.getTitle());
            articleViewHolder.tvDescription.setText(article.getDescription());
        } else if (holder instanceof ProgressBarViewHolder) {
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (articleList.get(position) != null) ? VIEW_ITEM : VIEW_PROG;
    }

    public void addMoreItems(List<ArticleResponse.Article> articleList) {
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }

    public void clearList() {
        this.articleList.clear();
    }

    public ArticleResponse.Article getArticle(int pos) {
        return this.articleList.get(pos);
    }

    public void updateItemOf(ArticleResponse.Article article) {

        for (ArticleResponse.Article temp : this.articleList) {
            if (temp.getId() == article.getId()) {
                temp.setTitle(article.getTitle());
                int position = this.articleList.indexOf(temp);
                notifyItemChanged(position);
                return;
            }
        }

    }

    public void addProgressBar(){
        articleList.add(null);
        notifyItemInserted(articleList.size() - 1);
    }

    public void removeProgressBar(){
        articleList.remove(articleList.size() - 1);
        notifyItemRemoved(articleList.size() - 1);
    }

    public int size(){
        return this.articleList.size();
    }


    /**
     * View Holder
     */
    class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.tvDescription)
        TextView tvDescription;

        @BindView(R.id.ivMore)
        ImageView ivMore;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ivMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onClicked(getAdapterPosition(), view);
        }
    }

    class ProgressBarViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressBar)
        ProgressBar progressBar;

        public ProgressBarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
